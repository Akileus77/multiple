<?php

require_once 'data/config.php';

if($_POST){

    $eposta     = $_POST['eposta'];
    $sifre      = $_POST['sifre'];
    $sifrele    = sha1(md5($sifre));

    if(!$eposta || !$sifre){
    echo "bos";
    }else{

        $girisyap = $db->prepare("SELECT * FROM uyeler WHERE eposta=:e AND sifre=:s");
        $girisyap->execute([':e'=>$eposta,':s'=>$sifrele]);

        if($girisyap->rowCount()){

            $row = $girisyap->fetch(PDO::FETCH_OBJ);

            $_SESSION['oturum'] = true;
            $_SESSION['kadi']   = $row->kadi;
            $_SESSION['eposta'] = $row->eposta;

            echo "ok";
            header("Refresh:2; url=giris.php");

        }else{
            echo "hata";
        }

    }

}

?>