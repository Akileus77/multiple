<?php    require_once 'components/header.php'; 

require_once 'data/config.php'; 

//print_r($_SESSION);

if(isset($_GET['cikis'])){
  session_destroy();
  echo '<div class="alert alert-success">Başarıyla çıkış yaptınız...</div>';
  header('refresh:2;url='.$_SERVER['HTTP_REFERER']);
}


?>
<!doctype html>
<html lang="tr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    


    <title>Giris</title>
 <!-- jQuery -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    <!-- styles -->


  </head>
  <style>


@import url('https://fonts.googleapis.com/css?family=Numans');

html,body{
background: rgb(34,34,205);
background: linear-gradient(90deg, rgba(34,34,205,1) 28%, rgba(23,71,152,1) 45%, rgba(41,142,203,1) 74%);
background-size: cover;
background-repeat: no-repeat;
height: 100%;
font-family: 'Numans', sans-serif;
}

.container{
height: 85%;
align-content: center;
}

.card{
height: 370px;
margin-top: auto;
margin-bottom: auto;
width: 400px;
background-color: rgba(0,0,0,0.5) !important;
}



.card-header h3{
color: black;
text-align: center;
}



.input-group-prepend span{
width: 50px;
background-color: #136bd4;
color: black;
border:0 !important;
}

input:focus{
outline: 0 0 0 0  !important;
box-shadow: 0 0 0 0 !important;

}



.login_btn{
color: black;
background-color: white;
margin: 10px;
width: 150px;
}

.login_btn:hover{
color: white;
background-color: black;
}

.links{
color: white;
}

.links a{
margin-left: 4px;
}



  </style>
  <body>
  <?php if(!isset($_SESSION['oturum'])){ ?>
  <?php }else{
    echo "<div style='color: white;'>Giriş Yapıldı</div>";
  }
?>
<br>
  <script>                 
                            function giris(){
                                var deger = $("#girisformu").serialize();
                                $.ajax({              
                                    type : "POST",
                                    data : deger,
                                    url : "girisyap.php",
                                    success : function(girisyap){
                                        if($.trim(girisyap) == "bos" ){
                                            sweetAlert('Hata','Boş alan bırakmayınız','error');
                                        }else if($.trim(girisyap) == "var"){
                                            sweetAlert('Hata','Bu e-posta adresi zaten kayıtlı','error');
                                        }else if($.trim(girisyap) == "ok"){
                                          
                                            sweetAlert('Başarılı','bir şekilde giriş yaptınız','success');
                                            
                                        }else if($.trim(girisyap) == "hata"){
                                            sweetAlert('Hata','E-posta adresiniz veya şifreniz yanlış','error');          
                                        }
                                    }
                                });
                            }
                            </script>
<?php if(!isset($_SESSION['oturum'])){ 
  ?>
  <div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div style="background-color: #fff;" class="card-header">
				<h3>Multiple.gg</h3>
			</div>
			<div class="card-body">
			<form action=""  id="girisformu" method="POST" onsubmit="return false;">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="email" name="eposta" class="form-control"  placeholder="E-posta ">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
            <input type="password" name="sifre" class="form-control"  placeholder="Şifre">
					</div>

					<div class="form-group">
          <button type="submit"  onclick="giris();"  class="btn float-right login_btn">Giriş Yap</button>
					</div>
				</form>
			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
        Hesabınız yok mu?<a href="kayit.php">Kayıt Ol</a>
				</div>
				<div class="d-flex justify-content-center">
					<a href="sifremiunutum.php">Parolanızı mı unuttunuz?</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php }else{
  echo "<p style='color: white;'>Hoşgeldiniz</> | ".$_SESSION['kadi']." | <a style='color: white;' href='giris.php?cikis'>Çıkış Yap</a>";
} 
?>


  </body>
</html>











