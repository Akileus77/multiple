<?php    require_once 'components/header.php';  ?>
<?php    require_once 'data/config.php';  

//print_r($_SESSION);

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    


    <title>Giris</title>

    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="assets/sweetalert.min.js"></script>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/sweetalert.css">

  </head>
  <style>


@import url('https://fonts.googleapis.com/css?family=Numans');

html,body{
background: rgb(34,34,205);
background: linear-gradient(90deg, rgba(34,34,205,1) 28%, rgba(23,71,152,1) 45%, rgba(41,142,203,1) 74%);
background-size: cover;
background-repeat: no-repeat;
height: 100%;
font-family: 'Numans', sans-serif;
}

.container{
height: 85%;
align-content: center;
}

.card{
height: 370px;
margin-top: auto;
margin-bottom: auto;
width: 400px;
background-color: rgba(0,0,0,0.5) !important;
}



.card-header h3{
color: black;
text-align: center;
}



.input-group-prepend span{
width: 50px;
background-color: #136bd4;
color: black;
border:0 !important;
}

input:focus{
outline: 0 0 0 0  !important;
box-shadow: 0 0 0 0 !important;

}



.login_btn{
color: black;
background-color: white;
margin: 10px;
width: 150px;
}

.login_btn:hover{
color: white;
background-color: black;
}

.links{
color: white;
}

.links a{
margin-left: 4px;
}


  </style>
  <body>

  <script>                 
                            function kayit(){
                                var bilgiler = $("#kayitformu").serialize();
                                $.ajax({              
                                    type : "POST",
                                    data : bilgiler,
                                    url : "islemler.php",
                                    success : function(kayitol){
                                        if($.trim(kayitol) == "bos" ){

                                            sweetAlert('Hata','Boş alan bırakmayınız','error');
                                        }else if($.trim(kayitol) == "var"){
                                            sweetAlert('Hata','Bu e-posta adresi zaten kayıtlı','error');
                                        }else if($.trim(kayitol) == "ok"){
                                            
                                            sweetAlert('Başarılı','Başarılı bir şekilde kayıt oldunuz','success');
                                        }else if($.trim(kayitol) == "hata"){
                                            sweetAlert('Hata','Sistemsel bir arıza oluştu','error');          
                                        }
                                    }
                                });
                            }                         
                            </script>


<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div style="background-color: #fff;" class="card-header">
				<h3>Multiple.gg</h3>
			</div>
			<div class="card-body">
      <form action=""  id="kayitformu" method="POST" onsubmit="return false;">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" name="kadi" class="form-control"  placeholder="Kullanıcı adınızı giriniz ">						
          </div>

          <div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="email" name="eposta" class="form-control"  placeholder="E-posta adresinizi giriniz">						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
            <input type="password" name="sifre" class="form-control"  placeholder="Şifrenizi giriniz">
					</div>

					<div class="form-group">
          <button type="submit" onclick="kayit();" class="btn float-right login_btn">Kayıt Ol</button>
					</div>
				</form>
			
		</div>
	</div>
</div>




  </body>
</html>



